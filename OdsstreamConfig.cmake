# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella

set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIR /usr/local/include/odsstream)
set(OdsStream_LIBRARY /usr/local/lib/libodsstream.so.0.7.6)


if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION "/usr/local/lib/libodsstream.so.0.7.6"
		INTERFACE_INCLUDE_DIRECTORIES "/usr/local/include/odsstream"
		)
endif()
