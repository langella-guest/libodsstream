/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2017  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
/**
 * \file ods2csv.h
 * \date 12/1/2017
 * \author Olivier Langella
 * \brief convert ods files to TSV
 */


#ifndef ODS2CSV_H
#define ODS2CSV_H

#include <QDebug>

#include <QObject>
#include <QCoreApplication>
#include "config.h"

class Ods2Csv : public QObject
{
  Q_OBJECT

  private:
  QCoreApplication *app;

  public:
  explicit Ods2Csv(QObject *parent = 0);
  /////////////////////////////////////////////////////////////
  /// Call this to quit application
  /////////////////////////////////////////////////////////////
  void quit();

  signals:
  /////////////////////////////////////////////////////////////
  /// Signal to finish, this is connected to Application Quit
  /////////////////////////////////////////////////////////////
  void finished();

  public slots:
  /////////////////////////////////////////////////////////////
  /// This is the slot that gets called from main to start everything
  /// but, everthing is set up in the Constructor
  /////////////////////////////////////////////////////////////
  void run();

  /////////////////////////////////////////////////////////////
  /// slot that get signal when that application is about to quit
  /////////////////////////////////////////////////////////////
  void aboutToQuitApp();
};

#endif // ODS2CSV_H
