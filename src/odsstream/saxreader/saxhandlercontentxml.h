/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QXmlDefaultHandler>
#include <QString>
#include <vector>
#include "../odsdocreader.h"

class SaxHandlerContentXml : public QXmlDefaultHandler
{

  public:
  SaxHandlerContentXml(OdsDocReader &);
  virtual ~SaxHandlerContentXml();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);

  QString errorString() const;

  private:
  bool start_element_table_row(const QXmlAttributes &attributes) const;
  bool end_element_table_row() const;
  bool start_element_table(const QXmlAttributes &attributes) const;
  bool end_element_table() const;
  bool start_element_table_cell(const QXmlAttributes &attributes);
  bool end_element_table_cell();
  bool start_element_p(const QXmlAttributes &attributes);
  bool end_element_p();
  bool end_element_a();
  bool start_element_office_annotation(const QXmlAttributes &attributes);
  bool end_element_office_annotation();
  /// the error description variable
  QString _errorStr;

  QString _currentText;

  std::vector<QString> _tag_stack;

  OdsDocReader &_ods_reader;

  uint _number_columns_repeated;

  OdsCell _current_cell;

  bool _is_annotation = false;
};
