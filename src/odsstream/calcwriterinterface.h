/**
 * \file odsstream/odsdochandlerinterface.h
 * \date 10/03/2015
 * \author Olivier Langella
 * \brief interface to use as a wildcard to writer in either TSV, ODS
 * TSVdirectory writers any kind of writer can be created in your program :
 * OdsDocWriter, TsvDirectoryWriter, TsvOutputStream
 * All of them implements this interface :
 * it allows to use the same code to write in these different formats.
 */

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2013  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QDate>
#include <QUrl>
#include "writer/options/odstablecellstyle.h"
#include "writer/options/odstablecellstyleref.h"
#include "writer/options/odscolorscale.h"
#include "writer/options/odstablesettings.h"

class CalcWriterInterface
{
  public:
  virtual ~CalcWriterInterface(){};
  virtual void close() = 0;

  /**
   * \brief open a new sheet
   *
   * \param sheetName the sheet name
   */
  virtual void writeSheet(const QString &sheetName) = 0;

  /**
   * \brief open a new line
   */
  virtual void writeLine() = 0;
  /**
   * \brief write a text cell
   * \param cell_text cell text
   */
  virtual void writeCell(const char *cell_text) = 0;
  /**
   * \brief write a text cell
   * \param cell_text cell text
   */
  virtual void writeCell(const QString &cell_text) = 0;
  /**
   * \brief write an empty cell
   */
  virtual void writeEmptyCell() = 0;
  /**
   * \brief write an unsigned integer in a cell
   * \param number integer to write
   */
  virtual void writeCell(std::size_t number) = 0;
  /**
   * \brief write an integer in a cell
   * \param number integer to write
   */
  virtual void writeCell(int number) = 0;
  /**
   * \brief write a positive integer in a cell
   * \param number integer to write
   */
  virtual void
  writeCell(unsigned int posInteger)
  {
    writeCell((std::size_t)posInteger);
  };
  /**
   * \brief write a float in a cell
   * \param number float to write
   */
  virtual void writeCell(float number) = 0;
  /**
   * \brief write a double in a cell
   * \param number double to write
   */
  virtual void writeCell(double number) = 0;

  /**
   * \brief write a double as a percentage
   * \param number double to write must be a ratio (0.5 == 50%)
   */
  virtual void writeCellPercentage(double value) = 0;
  /**
   * \brief write a boolean in a cell
   * \param true_or_false boolean to write
   */
  virtual void writeCell(bool true_or_false) = 0;
  /**
   * \brief write a date in a cell
   * \param date date to write
   */
  virtual void writeCell(const QDate &date) = 0;
  /**
   * \brief write a timestamp in a cell
   * \param datetime timestamp to write
   */
  virtual void writeCell(const QDateTime &datetime) = 0;
  /**
   * \brief write a text cell with an URL link
   * \param url_link URL link
   * \param text text to write
   */
  virtual void writeCell(const QUrl &url_link, const QString &text) = 0;


  /**
   * build table cell style reference with a style definition
   *
   * \param style OdsTableCellStyle
   * \return OdsTableCellStyleRef pointer on a style reference
   */
  virtual OdsTableCellStyleRef
  getTableCellStyleRef(const OdsTableCellStyle &style)
  {
    return nullptr;
  };
  /**
   * set the cell table style. This is applied to in the stream to following
   * cells. This ends by using an other style reference or by using
   * setTableCellStyleRef function
   *
   * \param style_ref OdsTableCellStyleRef
   */
  virtual void setTableCellStyleRef(OdsTableCellStyleRef style_ref){};

  /**
   * clear cell style definition in the stream. the default style will be
   * applied.
   *
   */
  void
  clearTableCellStyleRef()
  {
    setTableCellStyleRef(nullptr);
  };

  /**
   * \brief set annotation to write in the next cell
   * \param annotation any comment on this cell
   */
  virtual void setCellAnnotation(const QString &annotation) = 0;

  /** \brief apply solor scale conditional format on a cell range
   *
   */
  virtual void addColorScale(const OdsColorScale &ods_color_scale){};

  /** @brief get the last written cell coordinate in ODS coordinate format
   * get the coordinate of the last written cell or an empty string if the
   * writer is not an OdsDocWriter
   *
   * @return QString with ODS cell coordinate of the form : "classeur.A5"
   */
  virtual QString
  getOdsCellCoordinate()
  {
    return QString();
  };

  /** @brief set ODS table settings of the current sheet (table)
   */
  virtual void setCurrentOdsTableSettings(const OdsTableSettings &settings){};
};
