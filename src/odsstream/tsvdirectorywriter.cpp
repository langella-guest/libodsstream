/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QFileInfo>
#include <QDebug>
#include "tsvdirectorywriter.h"
#include "odsexception.h"


TsvDirectoryWriter::TsvDirectoryWriter()
{
}

TsvDirectoryWriter::TsvDirectoryWriter(const QDir &directory)
  : _directory(directory)
{
  if(_directory.exists())
    {
    }
  else
    {
      if(!_directory.mkdir(_directory.absolutePath()))
        {
          throw OdsException(QString("unable to create output directory %1")
                               .arg(_directory.absolutePath()));
        }
    }
}

TsvDirectoryWriter::~TsvDirectoryWriter()
{
  close();
}

void
TsvDirectoryWriter::setSeparator(TsvSeparator separator)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  switch(separator)
    {
      case TsvSeparator::tab:
        _separator = "\t";
        break;
      case TsvSeparator::comma:
        _separator = ",";
        break;
      case TsvSeparator::semicolon:
        _separator = ";";
        break;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
TsvDirectoryWriter::close()
{
  if(_p_otxtstream != nullptr)
    {
      *_p_otxtstream << _end_of_line;
      _p_otxtstream->flush();
      //_p_otxtstream->close();
      delete(_p_otxtstream);
      _p_otxtstream = nullptr;
    }
  if(_p_ofile != nullptr)
    {
      _p_ofile->flush();
      _p_ofile->close();
      delete(_p_ofile);
      _p_ofile = nullptr;
    }
}

void
TsvDirectoryWriter::writeSheet(const QString &sheetName)
{
  close();
  // qDebug() << " TsvDirectoryWriter::writeSheet " <<
  // _directory.absolutePath();
  _p_ofile = new QFile(QString(_directory.absolutePath())
                         .append("/")
                         .append(sheetName)
                         .append(this->_file_extension));
  // qDebug() << " TsvDirectoryWriter::writeSheet " <<
  // QFileInfo(*_p_ofile).absoluteFilePath();
  if(_p_ofile->open(QIODevice::WriteOnly))
    {
      _p_otxtstream = new QTextStream(_p_ofile);
    }
  else
    {
      throw OdsException(QString("unable to write into file %1")
                           .arg(QFileInfo(*_p_ofile).absoluteFilePath()));
    }
  _tableRowStart = true;
  _startingSheet = true;
}


void
TsvDirectoryWriter::ensureSheet()
{
  if(_p_otxtstream == nullptr)
    {
      writeSheet("default");
    }
}

void
TsvDirectoryWriter::writeLine()
{
  ensureSheet();
  if(_startingSheet)
    {
    }
  else
    {
      *_p_otxtstream << _end_of_line;
    }
  _startingSheet = false;
  _tableRowStart = true;
}
void
TsvDirectoryWriter::writeCell(const char *cstr)
{
  writeCell(QString(cstr));
}
void
TsvDirectoryWriter::writeCell(const QString &text)
{
  ensureSheet();
  if(!_tableRowStart)
    {
      *_p_otxtstream << _separator;
    }
  _tableRowStart = false;

  if(m_quoteStrings)
    {
      *_p_otxtstream << QString("\"%1\"").arg(
        QString(text).replace("\"", "\"\""));
    }
  else
    {
      if(text.contains(_end_of_line) || text.contains(_separator) ||
         text.contains("\""))
        {
          *_p_otxtstream << QString("\"%1\"").arg(
            QString(text).replace("\"", "\"\""));
        }
      else
        {
          *_p_otxtstream << text;
        }
    }

  _startingSheet = false;
}

void
TsvDirectoryWriter::writeRawCell(const QString &text)
{
  ensureSheet();
  if(!_tableRowStart)
    {
      *_p_otxtstream << _separator;
    }
  _tableRowStart = false;
  *_p_otxtstream << text;
  _startingSheet = false;
}
void
TsvDirectoryWriter::writeEmptyCell()
{
  ensureSheet();
  if(!_tableRowStart)
    {
      *_p_otxtstream << _separator;
    }
  _tableRowStart = false;
  _startingSheet = false;
}
void
TsvDirectoryWriter::writeCell(std::size_t num)
{
  writeCell(QString::number(num, 'g', 10));
}
void
TsvDirectoryWriter::writeCell(int num)
{
  writeRawCell(QString::number(num, 'g', 10));
}
void
TsvDirectoryWriter::writeCell(float num)
{
  writeRawCell(QString::number(num, 'g', numFloatPrecision));
}
void
TsvDirectoryWriter::writeCell(double num)
{
  writeRawCell(QString::number(num, 'g', numFloatPrecision));
}

void
TsvDirectoryWriter::writeCellPercentage(double value)
{
  writeCell(value);
}
void
TsvDirectoryWriter::writeCell(bool isOk)
{
  if(isOk)
    {
      writeRawCell(QString("T"));
    }
  else
    {
      writeRawCell(QString("F"));
    }
}
void
TsvDirectoryWriter::writeCell(const QDate &date)
{
  writeRawCell(date.toString("yyyy-MM-dd"));
}
void
TsvDirectoryWriter::writeCell(const QDateTime &date)
{
  writeRawCell(date.toString("yyyy-MM-ddThh:mm:ss"));
}
void
TsvDirectoryWriter::writeCell(const QUrl &url, const QString &text)
{
  writeCell(text);
}


void
TsvDirectoryWriter::setQuoteStrings(bool quote_strings)
{
  m_quoteStrings = quote_strings;
}
