/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once


#include <QString>
#include <QUrl>
#include <QDate>
#include <QDir>
#include <QTextStream>
#include <QFile>
#include "calcwriterinterface.h"
#include "config.h"

class TsvDirectoryWriter : public CalcWriterInterface
{
  public:
  TsvDirectoryWriter(const QDir &directory);
  virtual ~TsvDirectoryWriter();

  void close() override;

  void writeSheet(const QString &sheetName) override;
  void writeLine() override;
  void writeCell(const char *) override;
  void writeCell(const QString &) override;
  void writeEmptyCell() override;
  void writeCell(std::size_t) override;
  void writeCell(int) override;
  void writeCell(float) override;
  void writeCell(double) override;
  void writeCellPercentage(double value) override;
  void writeCell(bool) override;
  void writeCell(const QDate &) override;
  void writeCell(const QDateTime &) override;
  void writeCell(const QUrl &, const QString &) override;
  void setCellAnnotation(const QString &annotation) override{};

  virtual void setSeparator(TsvSeparator separator);
  virtual void setQuoteStrings(bool quote_strings);

  protected:
  TsvDirectoryWriter();
  void writeRawCell(const QString &text);

  QString _separator             = "\t";
  QString _end_of_line           = "\n";
  QTextStream *_p_otxtstream     = nullptr;
  unsigned int numFloatPrecision = 12;

  private:
  const QDir _directory;

  QString _file_extension = ".tsv";

  bool _tableRowStart = true;
  bool _startingSheet = false;
  bool m_quoteStrings    = false;

  QFile *_p_ofile = nullptr;
  void ensureSheet();
};

