/**
 * \file odsstream/writer/options/odstablesettings.h
 * \date 23/04/2018
 * \author Olivier Langella
 * \brief store table settings
 */

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the libodsstream library.
 *
 *    libodsstream is a library to read and write ODS documents as streams
 *    Copyright (C) 2013  Olivier Langella <Olivier.Langella@u-psud.fr>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *by the Free Software Foundation, either version 3 of the License, or (at your
 *option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef ODSTABLESETTINGS_H
#define ODSTABLESETTINGS_H

#include <QString>
#include <map>

class SettingsXml;

class OdsTableSettings
{
  friend SettingsXml;

  public:
  OdsTableSettings();
  OdsTableSettings(const OdsTableSettings &other);
  virtual ~OdsTableSettings();

  OdsTableSettings &operator=(OdsTableSettings const &other);
  void setVerticalSplit(unsigned int position);
  void setHorizontalSplit(unsigned int position);

  const QString &getSheetName() const;

  protected:
  void setSheetName(const QString &sheetname);
  const QString getValue(const QString &property) const;

  private:
  QString _sheetname;
  std::map<QString, QString> _map_properties;
};

#endif // ODSTABLESETTINGS_H
